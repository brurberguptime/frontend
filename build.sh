#!/bin/sh
CONTAINER="dethsanius/brurberg_frontend"
DEPLOYMENT_PATH="../kubernetes/frontend/deployment.yaml"

set -e
if (( $# < 1 ))
then
    printf "%b" "Error. Not enough arguments.\n" >&2
    printf "%b" "usage: build.sh <version> [<message>]\n" >&2
    exit 1
elif (( $# > 2 ))
then
    printf "%b" "Error. Too many arguments.\n" >&2
    printf "%b" "usage: build.sh <version> [<message>]\n" >&2
    exit 2
fi

docker build -t $CONTAINER:$1 .
docker push $CONTAINER:$1
sed -E -i "s|$CONTAINER:[0-9]+.[0-9]+.[0-9]+|$CONTAINER:$1|g" $DEPLOYMENT_PATH
kubectl apply -f $DEPLOYMENT_PATH

if (( $# == 2 ))
then
  git add -A
  git commit -am "$2"
  git tag v$1
  git push
  git push --tags
fi
